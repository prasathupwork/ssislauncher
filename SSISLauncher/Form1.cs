﻿using Microsoft.SqlServer.Dts.Runtime;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Windows.Forms;

namespace SSISLauncher
{
    public partial class Form1 : Form
    {
        private const string FileName = "defaults.json";
        private PackageDetail package;
        private List<PackageDetail> packages;

        public Form1()
        {
            InitializeComponent();

            label5.Text = string.Empty;
            textBox2.Enabled = false;

            LoadDefault();

            LoadControls();

            BindDefaults();
        }

        private void LoadDefault()
        {
            if (File.Exists(FileName))
            {
                var data = File.ReadAllText(FileName);

                packages = JsonConvert.DeserializeObject<List<PackageDetail>>(data) ?? new List<PackageDetail>();

                if (packages.Count == 1)
                {
                    package = packages.First();
                }
                else if (packages.Count > 1)
                {
                    package = packages.FirstOrDefault(e => e.IsDefault);
                }

                if (package == null)
                {
                    package = new PackageDetail();
                    packages.Add(package);
                }
            }
            else
            {
                packages = new List<PackageDetail>();
                package = new PackageDetail();

                packages.Add(package);
            }
        }

        private void LoadControls()
        {
            var packageNames = packages.Where(e => e.PackageName != null).Select(e => new PackageVariable() { Name = e.PackageName }).ToList() ?? new List<PackageVariable>(); ;
            packageNames.Insert(0, new PackageVariable() { Name = "< add new >" });

            comboBox1.DataSource = packageNames;
            comboBox1.DisplayMember = "Name";
            comboBox1.ValueMember = "Name";

            BindGrid();
        }

        private void BindDefaults(bool skipCombo = false)
        {
            if (package.PackageName != null)
            {
                textBox1.Text = package.PackagePath;
                if (!skipCombo)
                {
                    comboBox1.SelectedValue = package.PackageName;
                }
                checkBox1.Checked = package.IsDefault;
                comboBox2.SelectedIndex = package.PackageType != null ? comboBox2.Items.IndexOf(package.PackageType) : 0;
                textBox2.Text = package.PackageToExecute;

                dataGridView1.DataSource = package.Variables;

                label5.Text = string.Empty;
            }
        }

        private void Reset()
        {
            textBox1.Text = package.PackageName;
            dataGridView1.DataSource = package.Variables;
            checkBox1.Checked = package.IsDefault;
            textBox2.Text = package.PackageToExecute;
            comboBox2.SelectedIndex = 0;
            label5.Text = string.Empty;
        }

        private void BindGrid()
        {
            dataGridView1.Columns.Clear();

            dataGridView1.AutoGenerateColumns = false;

            DataGridViewCell cell = new DataGridViewTextBoxCell();
            DataGridViewTextBoxColumn colVariableName = new DataGridViewTextBoxColumn()
            {
                CellTemplate = cell,
                Name = "VariableName",
                HeaderText = "Variable Name",
                DataPropertyName = "Name",
                AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill
            };

            dataGridView1.Columns.Add(colVariableName);

            DataGridViewCell cell1 = new DataGridViewTextBoxCell();
            DataGridViewTextBoxColumn colVariableValue = new DataGridViewTextBoxColumn()
            {
                CellTemplate = cell1,
                Name = "Value",
                HeaderText = "Value",
                DataPropertyName = "Value",
                AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill
            };

            dataGridView1.Columns.Add(colVariableValue);


        }

        private DTSExecResult ExecutePackage()
        {
            MyEventListener listener = new MyEventListener();
            var myApplication = new Microsoft.SqlServer.Dts.Runtime.Application();

            Package myPackage = myApplication.LoadPackage(package.PackagePath, listener);

            foreach (var item in package.Variables)
            {
                myPackage.Variables[item.Name].Value = item.Value;
            }

            return myPackage.Execute(null, null, listener, null, null);
        }

        private DTSExecResult ExecuteProject()
        {
            DTSExecResult resp;
            MyEventListener listener = new MyEventListener();
            var myApplication = new Microsoft.SqlServer.Dts.Runtime.Application();

            var proj = Project.OpenProject(package.PackagePath, listener);

            var packageToExe = proj.PackageItems.Where(e=>e.StreamName == package.PackageToExecute).FirstOrDefault();

            if (packageToExe != null)
            {
                var pk = packageToExe.Package;

                foreach (var item in package.Variables)
                {
                    pk.Parameters[item.Name].Value = item.Value;
                }

                resp = pk.Execute(null,null,listener,null,null);
            }
            else
            {
                label5.Text = "Package not found in project";
                resp = DTSExecResult.Failure;
            }

            proj.Dispose();

            return resp;
        }

        private void button1_Click(object sender, System.EventArgs ev)
        {
            button1.Enabled = false;

            package.IsDefault = checkBox1.Checked;
            package.PackageName = comboBox1.Text;
            package.PackagePath = textBox1.Text;
            package.PackageToExecute = textBox2.Text;
            package.PackageType = comboBox2.Text;

            var exists = packages.Any(e => e.PackageName == package.PackageName && package.Id != e.Id);
            if (exists)
            {
                var result = MessageBox.Show("Package already exists. Override ?", "Error", MessageBoxButtons.YesNo);

                if (result == DialogResult.Yes)
                {
                    packages.RemoveAll(e => e.PackageName == package.PackageName);
                    package.Id = Guid.NewGuid();
                    packages.Add(package);
                }
                else
                {
                    return;
                }
            }

            if (package.IsDefault)
            {
                packages.ForEach(e => e.IsDefault = false);
                package.IsDefault = true;
            }

            File.WriteAllText(FileName, JsonConvert.SerializeObject(packages));


            label5.Text = string.Format("Running package '{0}'", package.PackageName);

            try
            {
                DTSExecResult res = DTSExecResult.Failure;

                if (package.PackageType.ToLower() == "project")
                {
                    res = ExecuteProject();
                }
                else if(package.PackageType.ToLower() == "multipackage")
                {
                    res = ExecuteMultiPackage();
                }
                else
                {
                    res = ExecutePackage();
                }

                label5.Text = string.Format("Completed with result: {0}", res.ToString());
            }
            catch (Exception ex)
            {
                string errorMsg = "An application error occurred. Please fix the below error and retry:\n\n";
                errorMsg = errorMsg + ex.Message + "\n\nStack Trace:\n" + ex.StackTrace;
                MessageBox.Show(errorMsg, "Execution Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            button1.Enabled = true;
        }

        private DTSExecResult ExecuteMultiPackage()
        {
            MyEventListener listener = new MyEventListener();
            var myApplication = new Microsoft.SqlServer.Dts.Runtime.Application();

            var packagesToExe = package.PackageToExecute.Split(';');
            var proj = Project.OpenProject(package.PackagePath, listener);

            DTSExecResult resp = DTSExecResult.Failure;

            foreach (var name in packagesToExe)
            {
                var packageToExe = proj.PackageItems.Where(e => e.StreamName == name).FirstOrDefault();

                if (packageToExe != null)
                {
                    var pk = packageToExe.Package;

                    foreach (var item in package.Variables)
                    {
                        if (pk.Parameters.Contains(item.Name))
                        {
                            pk.Parameters[item.Name].Value = item.Value;
                        }
                    }

                    resp = pk.Execute(null, null, listener, null, null);
                }
                else
                {
                    label5.Text = string.Format("Package {0} not found in project", name);
                    resp = DTSExecResult.Failure;
                    break;
                }

                proj.Dispose();
            }

            return resp;
        }

        private void comboBox1_SelectedIndexChanged(object sender, System.EventArgs ev)
        {
            var text = comboBox1.Text;

            if (text == "< add new >")
            {
                package = new PackageDetail();
                packages.Add(package);

                LoadControls();
                Reset();
            }
            else
            {
                if (packages != null)
                {
                    var tpackage = packages.FirstOrDefault(e => e.PackageName == text);
                    if (tpackage != null)
                    {
                        package = tpackage;
                        BindDefaults(true);
                    }
                }
            }
        }

        private void comboBox2_SelectedIndexChanged(object sender, EventArgs e)
        {
            if(comboBox2.Text.ToLower() == "project")
            {
                textBox2.Enabled = true;
            }
            else
            {
                textBox2.Enabled = false;
            }
        }
    }

    class MyEventListener : DefaultEvents
    {
        public override bool OnError(DtsObject source, int errorCode, string subComponent,
          string description, string helpFile, int helpContext, string idofInterfaceWithError)
        {
            // Add application-specific diagnostics here.
            Console.WriteLine("Error in {0}/{1} : {2}", source, subComponent, description);
            return false;
        }
    }
    }

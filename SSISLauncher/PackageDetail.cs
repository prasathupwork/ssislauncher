﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SSISLauncher
{
    public class PackageDetail
    {
        public PackageDetail()
        {
            Variables = new BindingList<PackageVariable>();
            Id = Guid.NewGuid();
        }

        public Guid Id { get; set; }
        public string PackageName { get; set; }
        public string PackageType { get; set; }
        public string PackageToExecute { get; set; }
        public string PackagePath { get; set; }
        public bool IsDefault { get; set; }
        public BindingList<PackageVariable> Variables { get; set; }
    }

    public class PackageVariable
    {
        public string Name { get; set; }
        public string Value { get; set; }
    }
}
